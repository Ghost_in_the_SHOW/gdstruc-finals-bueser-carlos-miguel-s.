#pragma once
#include <assert.h>
#include "UnorderedArray.h"

using namespace std;

template<class T>
class Stack
{
public:
	Stack(int size)
	{
		mGrowSize = 0;
		mContainer = new UnorderedArray<T>(size);
	}

	virtual ~Stack()
	{
		if (!mContainer->isEmpty())
			mContainer->~UnorderedArray();
	}

	void push(T value)
	{
		mContainer->pushFront(value);
		mGrowSize++;
	}

	void pop()
	{
		mContainer->popFront();
		mGrowSize--;

		if (mGrowSize <= 1)
			clearList();
	}

	const T& top()
	{
		if (!mContainer->isEmpty())
			return mContainer->top();
		else
			return NULL;
	}

	int getSize()
	{
		return mContainer->getSize();
	}

	void clearList()
	{
		mContainer->~UnorderedArray();
		mGrowSize = 1;
		mContainer = new UnorderedArray<T>(mGrowSize);
	}


private:
	UnorderedArray<T>* mContainer;
	int mGrowSize;
};