#include <iostream>
#include <string>
#include <time.h>
#include "Queue.h"
#include "Stack.h"

using namespace std;

void newScreen()
{
	cout << endl;
	system("pause");
	system("cls");
}

void printOptions()
{
	cout << "\n=========================\n";
	cout << "What do you want to do?\n";
	cout << "1 - Push elements\n";
	cout << "2 - Pop elements\n";
	cout << "3 - Print every element then empty set\n";
	cout << "4 - Exit Program\n";
}

int pickOption(int &input)
{
	while (true)
	{
		printOptions();
		cin >> input;

		switch (input)
		{
		case 1:
		case 2:
		case 3:
		case 4:
			return input;
		default:
			cout << "\nInvalid Input try again\n";
			newScreen();
		}
	}
	newScreen();
}

void pushInput(Stack<int> &stackList, Queue<int> &queueList)
{
	int input;
	cout << "Enter number:";
	cin >> input;

	stackList.push(input);
	queueList.push(input);
}

void popTop(Stack<int>& stackList, Queue<int>& queueList)
{
	stackList.pop();
	queueList.pop();
}

void emptySet(Stack<int>& stackList, Queue<int>& queueList)
{
	int loopSize = stackList.getSize();

	cout << "\nStack Elements\n";
	for (int i = 0; i < loopSize; i++)
	{
		cout << stackList.top() << endl;
		stackList.pop();
	}

	cout << "\nQueue Elements\n";
	for (int i = 0; i < loopSize; i++)
	{
		cout << queueList.top() << endl;
		queueList.pop();
	}

	stackList.clearList();
	queueList.clearList();
}

void printTop(Stack<int>& stackList, Queue<int>& queueList)
{
	cout << "\nTop element of set\n";

	if (stackList.top() != NULL && queueList.top() != NULL)
	{
		cout << "\nStack: " << stackList.top();
		cout << "\nQueue: " << queueList.top();
	}

	else
	{
		cout << "\nStack: ";
		cout << "\nQueue: ";
	}
	cout << endl;
}

int main()
{
	cout << "Enter size for element sets:";
	int size;
	cin >> size;

	Stack<int> stackList(size);
	Queue<int> queueList(size);
	bool progRun = true;
	
	do
	{
		int input;
		pickOption(input);
		
		switch (input)
		{
		case 1:
			pushInput(stackList, queueList);
			printTop(stackList, queueList);
			break;
		case 2:
			cout << "\nYou have popped the front element\n";
			popTop(stackList, queueList);
			printTop(stackList, queueList);
			break;
		case 3:
			emptySet(stackList, queueList);
			break;
		case 4:
			progRun = false;
			cout << "\nYou choose to exit program\n";
			break;
		}
		newScreen();
	} while (progRun);

	cout << "     Made by BUESER, CARLOS MIGUEL S.     \n          TG003          \n\n     Thank You for Playing";
	newScreen();

	return 0;
}